package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.model.IAbstractRepository;
import ru.t1.kharitonova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull M model) {
        entityManager.remove(model);
    }

}
