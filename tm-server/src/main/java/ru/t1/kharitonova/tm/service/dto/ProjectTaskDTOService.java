package ru.t1.kharitonova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.kharitonova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.ITaskDTOService;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.repository.dto.ProjectDTORepository;
import ru.t1.kharitonova.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    public IProjectDTOService projectService;

    @NotNull
    public ITaskDTOService taskService;

    public ProjectTaskDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectDTOService projectService,
            @NotNull final ITaskDTOService taskService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();

            @Nullable TaskDTO task = taskRepository.findOneById(taskId);
            @Nullable ProjectDTO project = projectRepository.findOneById(projectId);
            if (task == null) throw new TaskNotFoundException();
            if (project == null) throw new ProjectNotFoundException();

            entityManager.getTransaction().begin();
            task.setProjectId(projectId);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        try {
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : tasks) {
                taskRepository.removeOneById(task.getId());
            }
            projectRepository.removeOneById(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        if (index == null || index < 0 || index >= projectRepository.getSize()) throw new IndexIncorrectException();
        try {
            @Nullable ProjectDTO project = projectRepository.findOneByIndexByUserId(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = project.getId();
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);

            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : tasks) projectRepository.removeOneById(task.getId());
            projectRepository.removeOneByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        try {
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDTO task = taskRepository.findOneById(taskId);
            if (task == null) throw new TaskNotFoundException();

            entityManager.getTransaction().begin();
            task.setProjectId(null);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
