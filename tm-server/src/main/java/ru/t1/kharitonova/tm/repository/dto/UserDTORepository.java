package ru.t1.kharitonova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT e FROM UserDTO e WHERE e.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT e FROM UserDTO e WHERE e.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findOneById(@Nullable final String id) {
        @NotNull final String jpql = "SELECT e FROM UserDTO e WHERE e.id = :id";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Override
    public long getSize() {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM UserDTO")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void setPassword(@Nullable final String id, @NotNull final String passwordHash) {
        entityManager
                .createQuery("UPDATE UserDTO e SET e.passwordHash = :passwordHash WHERE e.id = :id")
                .setParameter("id", id)
                .setParameter("passwordHash", passwordHash)
                .executeUpdate();
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @NotNull final String jpql = "UPDATE UserDTO e SET e.firstName = :firstName " +
                "e.lastName = :lastName AND e.middleName = :middleName " +
                "WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .setParameter("firstName", firstName)
                .setParameter("lastName", lastName)
                .setParameter("middleName", middleName)
                .executeUpdate();
    }

    @Override
    public void lockUserById(@Nullable final String id) {
        @NotNull final String jpql = "UPDATE UserDTO e SET e.locked = true WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void unLockUserById(@Nullable final String id) {
        @NotNull final String jpql = "UPDATE UserDTO e SET e.locked = false WHERE e.id = :id";
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

}
