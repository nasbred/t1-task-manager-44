package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kharitonova.tm.dto.request.task.*;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.response.task.TaskCreateResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskListResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskShowByIdResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskUpdateByIdResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserLoginResponse;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = loginResponse.getToken();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(
                token, "Task bind test", "Task bind test");
        taskEndpoint.createTask(requestCreateTask);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTaskDTO().getId();
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                token, "Project bind test", "Project bind test");
        Assert.assertNotNull(projectEndpoint.createProject(requestCreateProject).getProjectDTO());
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProjectDTO().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.bindTaskToProject(request);

        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<TaskDTO> taskDTOList = responseList.getTaskDTOS();
        Assert.assertNotNull(taskDTOList);
        Assert.assertEquals(projectId, taskDTOList.get(1).getProjectId());
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(
                token, "Task unbind test", "Task unbind test");
        taskEndpoint.createTask(requestCreateTask);
        Assert.assertNotNull(taskEndpoint.createTask(requestCreateTask).getTaskDTO());
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTaskDTO().getId();

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                token, "Project unbind test", "Project unbind test");
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProjectDTO().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.unbindTaskToProject(requestUnbind);

        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<TaskDTO> taskDTOList = responseList.getTaskDTOS();
        Assert.assertNotNull(taskDTOList);
        Assert.assertNull(taskDTOList.get(0).getProjectId());
    }

    @Test
    public void testChangeTaskStatusById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        @Nullable final String taskId = taskDTO.getId();
        @Nullable final Status status = taskDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        @NotNull final Status newStatus = Status.IN_PROGRESS;
        taskDTO = taskEndpoint.changeStatusById(new TaskChangeStatusByIdRequest(token, taskId, newStatus)).getTaskDTO();
        Assert.assertNotNull(taskDTO);
        Assert.assertNotEquals(Status.NOT_STARTED, taskDTO.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, taskDTO.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testClearTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test task", "Test Description"));
        @Nullable List<TaskDTO> taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNotNull(taskDTOS);
        Assert.assertTrue(taskDTOS.size() > 0);
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNull(taskDTOS);
    }

    @Test
    public void testCreateTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testShowTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        @NotNull String taskId = taskDTO.getId();
        @NotNull final TaskShowByIdResponse taskGetByIdResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, taskId)
        );
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTaskDTO());
        Assert.assertEquals("Test task", taskGetByIdResponse.getTaskDTO().getName());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testListTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test1 task", "Test Description"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test2 task", "Test Description2"));
        @Nullable List<TaskDTO> taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNotNull(taskDTOS);
        Assert.assertEquals(2, taskDTOS.size());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testListTaskByProjectId() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test1 task", "Test Description"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test2 task", "Test Description2"));
        @Nullable List<TaskDTO> taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNotNull(taskDTOS);
        Assert.assertEquals(2, taskDTOS.size());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testRemoveTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(
                token, "First task", "Task Description"));
        @Nullable List<TaskDTO> taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNotNull(taskDTOS);
        Assert.assertEquals(1, taskDTOS.size());
        @NotNull final String taskId1 = taskDTOS.get(0).getId();
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskId1));
        taskDTOS = taskEndpoint.listTask(new TaskListRequest(token)).getTaskDTOS();
        Assert.assertNull(taskDTOS);
    }

    @Test
    public void testUpdateTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        @Nullable String taskId = taskDTO.getId();
        Assert.assertEquals("Test task", taskDTO.getName());
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, taskId, "Test task new", "Test Description New")
        );
        Assert.assertNotNull(taskUpdateByIdResponse);
        Assert.assertNotNull(taskUpdateByIdResponse.getTaskDTO());
        Assert.assertEquals(taskId, taskUpdateByIdResponse.getTaskDTO().getId());
        Assert.assertEquals("Test task new", taskUpdateByIdResponse.getTaskDTO().getName());
        Assert.assertEquals("Test Description New", taskUpdateByIdResponse.getTaskDTO().getDescription());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testStartTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        @Nullable final String taskId = taskDTO.getId();
        @Nullable final Status status = taskDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        taskDTO = taskEndpoint.startTaskById(new TaskStartByIdRequest(token, taskId)).getTaskDTO();
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(Status.IN_PROGRESS, taskDTO.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testCompleteTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable TaskDTO taskDTO = response.getTaskDTO();
        Assert.assertNotNull(taskDTO);
        @Nullable final String taskId = taskDTO.getId();
        @Nullable final Status status = taskDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        taskDTO = taskEndpoint.completeTaskById(new TaskCompleteByIdRequest(token, taskId)).getTaskDTO();
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(Status.COMPLETED, taskDTO.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

}
