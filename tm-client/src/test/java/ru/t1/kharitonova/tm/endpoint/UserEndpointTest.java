package ru.t1.kharitonova.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IUserEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.user.*;
import ru.t1.kharitonova.tm.dto.response.user.*;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.UUID;


@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = adminResponse.getToken();
    }

    @Test
    public void testChangePassword() {
        @NotNull UserLoginResponse response = authEndpoint.login(
                (new UserLoginRequest("test", "test"))
        );
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordResponse response1 = userEndpoint.changeUserPassword(
                new UserChangePasswordRequest(token, "newPassword")
        );
        Assert.assertNotNull(response1);
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        response = authEndpoint.login(new UserLoginRequest("test", "newPassword"));
        Assert.assertNotNull(response);
        @NotNull final UserChangePasswordResponse response2 = userEndpoint.changeUserPassword(
                new UserChangePasswordRequest(token, "test")
        );
    }

    @Test
    public void testLock() {
        @NotNull final UserLoginResponse response = authEndpoint.login(
                (new UserLoginRequest("test", "test"))
        );
        Assert.assertNotNull(response);
        userEndpoint.lockUser(new UserLockRequest(token, "test"));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        userEndpoint.unlockUser(new UserUnlockRequest(token, "test"));
    }

    @Test
    public void testRegistryUser() {
        @Nullable final UserRegistryResponse response = userEndpoint.registryUser(
                new UserRegistryRequest(token, "user_registry" + UUID.randomUUID().toString(), "user_registry")
        );
        Assert.assertNotNull(response.getUserDTO());
        @Nullable final UserDTO userDTO = response.getUserDTO();
        Assert.assertNotNull(userDTO);
        Assert.assertNotNull(userDTO.getLogin());
        Assert.assertNotNull(response.getUserDTO().getLogin());
        Assert.assertEquals(response.getUserDTO().getLogin(), userDTO.getLogin());
        authEndpoint.login(new UserLoginRequest(response.getUserDTO().getLogin(), "user_registry"));
        userEndpoint.removeUser(new UserRemoveRequest(token, response.getUserDTO().getLogin()));
    }

    @Test
    public void testRemoveUser() {
        @NotNull final UserRegistryResponse responseRegistry = userEndpoint.registryUser(
                new UserRegistryRequest(token, "user", "user")
        );
        Assert.assertNotNull(responseRegistry);
        Assert.assertNotNull(responseRegistry.getUserDTO());
        Assert.assertEquals("user", responseRegistry.getUserDTO().getLogin());
        @NotNull final UserLoginResponse response = authEndpoint.login(
                (new UserLoginRequest("user", "user"))
        );
        Assert.assertNotNull(response);
        userEndpoint.removeUser(new UserRemoveRequest(token, "user"));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("user", "user"))
        );
    }

    @Test
    public void testUnlockUser() {
        userEndpoint.lockUser(new UserLockRequest(token, "test"));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        userEndpoint.unlockUser(new UserUnlockRequest(token, "test"));
        @NotNull final UserLoginResponse response = authEndpoint.login(
                (new UserLoginRequest("test", "test"))
        );
        Assert.assertNotNull(response);
    }

    @Test
    public void testUpdateUserProfile() {
        @NotNull final UserLoginResponse response = authEndpoint.login((new UserLoginRequest("test", "test")));
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
        @NotNull UserProfileResponse response1 = authEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(response1);
        @Nullable UserDTO userDTO = response1.getUserDTO();
        Assert.assertNotNull(userDTO);
        @NotNull UserUpdateProfileResponse response2 = userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(token, "firstName", "lastName", "middleName")
        );
        Assert.assertNotNull(response2);
        userDTO = response2.getUserDTO();
        Assert.assertNotNull(userDTO);
        Assert.assertEquals("firstName", userDTO.getFirstName());
        Assert.assertEquals("lastName", userDTO.getLastName());
        Assert.assertEquals("middleName", userDTO.getMiddleName());
    }

}
