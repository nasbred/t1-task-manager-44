package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task extends AbstractUserOwnedModel {

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public Task(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final Status status,
            @Nullable final Project project
    ) {
        super(user);
        this.name = name;
        this.description = description;
        this.status = status;
        this.project = project;
    }

}
