package ru.t1.kharitonova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(@Nullable final String token) {
        super(token);
    }

    public UserRegistryRequest(
            @Nullable final String token,
            @Nullable final String login,
            @Nullable final String password
    ) {
        super(token);
        this.login = login;
        this.password = password;
    }
}
