package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @Column(name = "role")
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public Session(@NotNull final User user) {
        super(user);
    }

}
