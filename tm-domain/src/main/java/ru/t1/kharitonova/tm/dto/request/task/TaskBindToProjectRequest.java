package ru.t1.kharitonova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskBindToProjectRequest(@Nullable final String token) {
        super(token);
    }

    public TaskBindToProjectRequest(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
