package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public class Project extends AbstractUserOwnedModel {

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public Project(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final Status status
    ) {
        super(user);
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
